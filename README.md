# User Guide for Batch PSF analyser

## installation

Download the file "PSF_Analyser_.ijm" and place it in FIJI/ImageJ plugins folder

## before running the macro

* record PSF is a [Bio-formats-compatible](http://www.openmicroscopy.org/site/support/bio-formats5.3/supported-formats.html) image file format.
* gather all PSF images in a single folder
    * no other file should be present in the folder

## running the macro

* in FIJI/ImageJ, start the PSF_Analyser, e.g. using the "Plugins" menu
* on promt, specify (i) source and (ii) destination directory
    * you raw PSF image belong in the source directory
    * the macro output (PSFs, FWHM fit plots and Results-table) are stored in the destination directory
* draw a rectangular ROI around abead of your choice
    * use the same ROI on separate channels of the same image for correct pixel-shift-analysis


